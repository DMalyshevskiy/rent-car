create table t_car
(
	id int(10) unsigned auto_increment
		primary key,
	model_id int(10) unsigned not null,
	car_number varchar(8) null,
	constraint t_vehicle_id_uindex
	unique (id)
)
	comment 'т/с'
;

create index fk_t_vehicle_model_id
	on t_car (model_id)
;

create table t_car_rent_point
(
	rent_point_id int(10) unsigned not null,
	car_id int(10) unsigned not null
		primary key,
	constraint fk_t_car_rent_point_car
	foreign key (car_id) references t_car (id)
)
	comment 'Т/с доступные для аренды на точке выдачи'
;

create index idx_t_car_rent_point_rent_point
	on t_car_rent_point (rent_point_id)
;

create table t_city
(
	id int(10) unsigned auto_increment
		primary key,
	name varchar(1000) not null,
	constraint t_city_id_uindex
	unique (id)
)
	comment 'Города присутствия'
;

create table t_client
(
	id int(10) unsigned auto_increment
		primary key,
	last_name varchar(100) not null comment 'Фамилия',
	first_name varchar(100) not null comment 'Имя',
	middle_name varchar(100) null comment 'Отчество',
	driving_license_number mediumint not null comment 'Водительское удостоверение: номер',
	driving_license_seria smallint(6) not null comment 'Водительское удостоверение: серия',
	constraint t_client_id_uindex
	unique (id)
)
	comment 'Клиент'
;

create index uq_t_client_driving_license
	on t_client (driving_license_seria, driving_license_number)
;

create table t_mark
(
	id int(10) unsigned auto_increment
		primary key,
	name varchar(1000) not null,
	constraint t_mark_id_uindex
	unique (id)
)
	comment 'Марки т/с'
;

create table t_model
(
	id int(10) unsigned auto_increment
		primary key,
	mark_id int(10) unsigned not null,
	name varchar(1000) not null,
	constraint t_model_id_uindex
	unique (id),
	constraint fk_t_model_mark_id
	foreign key (mark_id) references t_mark (id)
)
	comment 'Модель т/с'
;

create index fk_t_model_mark_id
	on t_model (mark_id)
;

alter table t_car
	add constraint fk_t_vehicle_model_id
foreign key (model_id) references t_model (id)
;

create table t_model_tariff_plan
(
	model_id int(10) unsigned not null,
	tariff_plan_id int(10) unsigned not null,
	primary key (model_id, tariff_plan_id),
	constraint fk_t_model_tariff_plan_model
	foreign key (model_id) references t_model (id)
)
	comment 'Связь тарифных планов с моделями'
;

create index fk_t_model_tariff_plan_tp
	on t_model_tariff_plan (tariff_plan_id)
;

create table t_price
(
	id int(10) unsigned auto_increment
		primary key,
	tariff_plan_id int(10) unsigned not null,
	period_id int(10) unsigned not null,
	amount int not null comment 'Цена в копейках',
	name varchar(100) null
)
	comment 'Цена'
;

create index fk_t_price_period
	on t_price (period_id)
;

create index fk_t_price_tariff_plan
	on t_price (tariff_plan_id)
;

create table t_rent
(
	id int(10) unsigned auto_increment
		primary key,
	car_id int(10) unsigned not null,
	client_id int(10) unsigned null,
	rent_point_from_id int(10) unsigned not null comment 'Пункт выдачи т/с',
	rent_point_to_id int(10) unsigned null comment 'Пункт возврата т/с',
	date_from datetime not null comment 'Дата/время выдачи т/с в аренду',
	date_to datetime null comment 'Дата/время возврата т/с',
	user_create_id int(10) unsigned null comment 'Пользователь создавший запись',
	user_edit_id int(10) unsigned null comment 'Пользователь отредактировавший запись',
	status_id int(10) unsigned default '1' not null,
	pledge int null comment 'Залог в копейках',
	tariff_plan_id int(10) unsigned null,
	version mediumtext null,
	constraint t_rent_id_uindex
	unique (id),
	constraint fk_t_rent_car
	foreign key (car_id) references t_car (id),
	constraint fk_t_rent_client
	foreign key (client_id) references t_client (id)
)
	comment 'Запись об аренде т/с'
;

create index fk_t_rent_client
	on t_rent (client_id)
;

create index fk_t_rent_rp_to
	on t_rent (rent_point_to_id)
;

create index fk_t_rent_status
	on t_rent (status_id)
;

create index fk_t_rent_user_create
	on t_rent (user_create_id)
;

create index fk_t_rent_user_edit
	on t_rent (user_edit_id)
;

create index t_rent_from_index
	on t_rent (rent_point_from_id, date_from)
;

create index fk_t_rent_car
	on t_rent (car_id)
;

create index fk_t_rent_tariff_plan
	on t_rent (tariff_plan_id)
;

create table t_rent_point
(
	id int(10) unsigned auto_increment
		primary key,
	city_id int(10) unsigned not null,
	name varchar(1000) not null,
	address varchar(4000) null,
	constraint t_rent_point_id_uindex
	unique (id),
	constraint fk_rent_point_city_id
	foreign key (city_id) references t_city (id)
)
	comment 'Пункты выдачи автомобилей'
;

create index fk_rent_point_city_id
	on t_rent_point (city_id)
;

alter table t_car_rent_point
	add constraint fk_t_car_rent_point_rent_point
foreign key (rent_point_id) references t_rent_point (id)
;

alter table t_rent
	add constraint fk_t_rent_rp_from
foreign key (rent_point_from_id) references t_rent_point (id)
;

alter table t_rent
	add constraint fk_t_rent_rp_to
foreign key (rent_point_to_id) references t_rent_point (id)
;

create table t_tariff_plan
(
	id int(10) unsigned auto_increment
		primary key,
	name varchar(100) not null,
	date_begin datetime not null,
	date_end datetime not null,
	constraint t_tariff_plan_id_uindex
	unique (id)
)
	comment 'Тарифный план'
;

alter table t_model_tariff_plan
	add constraint fk_t_model_tariff_plan_tp
foreign key (tariff_plan_id) references t_tariff_plan (id)
;

alter table t_price
	add constraint fk_t_price_tariff_plan
foreign key (tariff_plan_id) references t_tariff_plan (id)
;

alter table t_rent
	add constraint fk_t_rent_tariff_plan
foreign key (tariff_plan_id) references t_tariff_plan (id)
;

create table t_user
(
	id int(10) unsigned auto_increment
		primary key,
	login varchar(30) not null,
	constraint t_user_id_uindex
	unique (id),
	constraint t_user_login_uindex
	unique (login)
)
	comment 'Пользователь'
;

alter table t_rent
	add constraint fk_t_rent_user_create
foreign key (user_create_id) references t_user (id)
;

alter table t_rent
	add constraint fk_t_rent_user_edit
foreign key (user_edit_id) references t_user (id)
;

create table t_user_points
(
	rent_point_id int(10) unsigned not null,
	user_id int(10) unsigned not null,
	primary key (rent_point_id, user_id),
	constraint fk_t_user_points_rp_id
	foreign key (rent_point_id) references t_rent_point (id),
	constraint fk_t_user_points_user_id
	foreign key (user_id) references t_user (id)
)
	comment 'Точки выдачи, обслуживаемые пользователем'
;

create index fk_t_user_points_user_id
	on t_user_points (user_id)
;

create table td_price_period
(
	id int(10) unsigned auto_increment
		primary key,
	name varchar(10) not null,
	seconds int(10) unsigned null comment 'Количество секунд в периоде',
	constraint td_price_period_id_uindex
	unique (id),
	constraint td_price_period_name_uindex
	unique (name)
)
	comment 'период тарификации'
;

alter table t_price
	add constraint fk_t_price_period
foreign key (period_id) references td_price_period (id)
;

create table td_rent_status
(
	id int(10) unsigned auto_increment
		primary key,
	name varchar(100) not null,
	constraint td_rent_status_id_uindex
	unique (id)
)
	comment 'Статус аренды т/с'
;

alter table t_rent
	add constraint fk_t_rent_status
foreign key (status_id) references td_rent_status (id)
;

