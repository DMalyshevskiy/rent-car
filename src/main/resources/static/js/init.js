var App;

$( function() {
    App = function () {
        var temp = {
            userId : 1, // TODO приделать авторизацию может быть
            rentpoints : [],
        };


        $.post({url:"/rentpoints", data : {}, success : function (resp) {
            temp.rentpoints = resp;
        }});

        return temp;
    }();

    // Инициализация вкладок
    $( "#tabs" ).tabs();

    // Датапикеры
    $('.date').datepicker();

    // Загрузка марок
    var $markSelect = $('#tabs-history').find('select.mark').html('<option value="">Выберите марку</option>');
    $.ajax({url:"/mark", success : function (resp) {
        $.each(resp, function (_, mark) {
            $markSelect.append('<option value="' + mark.id + '">' + mark.name +'</option>');
        });
    }});

    // Загрузка моделей
    var $carSelect = $('#tabs-history').find('select.car');
    var $modelSelect = $('#tabs-history').find('select.model').html('<option value="">Выберите модель</option>');

    $markSelect.change(function () {
        $carSelect.html('<option value="">Выберите номер</option>');

        $.ajax({url:"/model", data : {markId : $markSelect.val()}, success : function (resp) {

            $modelSelect.html('<option value="">Выберите модель</option>');

            $.each(resp, function (_, model) {
                $modelSelect.append('<option value="' + model.id + '">' + model.name +'</option>');
            });
        }});
    });

    // Загрузка номеров машин

    $modelSelect.change(function () {
        $.ajax({url: "/car", data: {modelId : $modelSelect.val()}, success : function (resp) {
            $carSelect.html('<option value="">Выберите номер</option>');

            if(!resp || !resp.length)
                return;

            $.each(resp, function (_, car) {
                $carSelect.append('<option value="' + car.id + '">' + car.number + '</option>');
            })
        }});
    });

    // Загрузка клиентов (для всех селектов)
    var $clientSelect = $('select.client').html('<option value="">Выберите клиента</option>');
    $.ajax({url : "/clients", success : function (resp) {
        if(!resp || !resp.length)
            return;

        $.each(resp, function (_, client) {
            $clientSelect.append('<option value = "' + client.id + '">' + client.lastName + ' ' + client.firstName + ' ' + client.middleName + '</option>');
        })
    }});

    // Загрузка точек приема-выдачи (для всех селектов)
    var $rentPointFrom = $('select.rentPointFrom').html('<option value="">Выберите пункт выдачи</option>')
    var $rentPointTo = $('select.rentPointTo').html('<option value="">Выберите пункт возврата</option>')

    $.ajax({url : "/rentpoints", success : function (resp) {
        if(!resp || !resp.length)
            return;
        $.each(resp, function (_, rp) {
            var html = '<option value="' + rp.id + '">' + rp.name + '</optionv>';
            $rentPointFrom.append(html);
            $rentPointTo.append(html);
        });

        $rentPointFrom.append()
    }});


    // Диалог редактирования
    // Загрузка марок
    var $markSelectSave = $('#history-edit').find('select.mark').html('<option value="">Выберите марку</option>');
    $.ajax({url:"/mark", success : function (resp) {
        $.each(resp, function (_, mark) {
            $markSelectSave.append('<option value="' + mark.id + '">' + mark.name +'</option>');
        });
    }});

    // Тарифные планы
    var modelTpMap = [];
    // Цены
    var priceMap = [];

    // Загрузка моделей
    var $carSelectSave = $('#history-edit').find('select.car');
    var $modelSelectSave = $('#history-edit').find('select.model').html('<option value="">Выберите модель</option>');

    $markSelectSave.change(function () {
        $carSelectSave.html('<option value="">Выберите номер</option>');

        $.ajax({url:"/model", data : {markId : $markSelectSave.val()}, success : function (resp) {

            $modelSelectSave.html('<option value="">Выберите модель</option>');

            $.each(resp, function (_, model) {
                $modelSelectSave.append('<option value="' + model.id + '">' + model.name +'</option>');
                modelTpMap[model.id] = model.tariffPlans;
            });
        }});
    });

    var $tariffPlanSelect = $('#history-edit').find('select.tariffPlan');

    // Загрузка номеров машин
    $modelSelectSave.change(function () {

        $tariffPlanSelect.html('<option value="">Выберите тарифный план</option>');
        var tpList = modelTpMap[$modelSelectSave.val()];
        if(tpList && tpList.length) {
            $.each(tpList, function (_, tp) {
                $tariffPlanSelect.append('<option value="' + tp.id + '">' + tp.name + '</option>');
                priceMap[tp.id] = tp.priceList;
            })
        }

        $.ajax({url: "/car", data: {
                                    modelId : $modelSelectSave.val(),
                                    rentPointId : saveDialog.isNew() ? $('#history-edit').find('select.rentPointFrom').val() : null,
                                    rentPointRequired : saveDialog.isNew()
                                    }, success : function (resp) {
            $carSelectSave.html('<option value="">Выберите номер</option>');

            if(!resp || !resp.length)
                return;

            $.each(resp, function (_, car) {
                $carSelectSave.append('<option value="' + car.id + '">' + car.number + '</option>');
            })
        }});
    });

    $('#history-edit').find('select.rentPointFrom').change(function () {
        $modelSelectSave.change();
    });

    var $priceDiv = $('#history-edit').find('div.tariffPlanPrices');
    $tariffPlanSelect.change(function () {
        $priceDiv.empty();
        var prices = priceMap[$tariffPlanSelect.val()];
        if(prices && prices.length) {
            var $ul = $('<ul></ul>');
            $.each(prices, function (_, price) {
                $ul.append('<li>' + (price.amount ? price.amount / 100 : '') + ' р./' + price.period.name + ' ' + price.name + '</li>');
            });
            $priceDiv.append($ul);
        }
    });

    // Загрузка статусов
    var $statusSelect = $('#history-edit').find('select.status');
    $.ajax({url : "/status", success : function (resp) {
        $statusSelect.html('<option value="">Выберите статус</option>');

        if(!resp || !resp.length)
            return;

        $.each(resp, function (_, status) {
            $statusSelect.append('<option value="' + status.id + '">' + status.name + '</option>');
        })
    }});

    var saveDialog = function() {
        var dialog = {};
        var $dialog = $('#history-edit');
        var rentId = null;

        var saveRent = function () {
            var params = {
                rentId : rentId,
                markId : $dialog.find('select.mark').val(),
                modelId : $dialog.find('select.model').val(),
                clientId : $dialog.find('select.client').val(),
                carId : $dialog.find('select.car').val(),
                dateFrom : $dialog.find('input.dateFrom').val(),
                dateTo : $dialog.find('input.dateTo').val(),
                rentPointFromId : $dialog.find('select.rentPointFrom').val(),
                rentPointToId : $dialog.find('select.rentPointTo').val(),
                status : $dialog.find('select.status').val(),
                pledge : $dialog.find('input.pledge').val(),
                tariffPlanId : $dialog.find('select.tariffPlan').val(),
                userCreateId : App.userId,
                userEditId : App.userId
            }
            console.log(params);
            // TODO проверить обязательные поля
            $.ajax({url : "/save-rent", data : params, success : function (resp) {
                console.log(resp);
                if(resp.isError) {
                    window.alert(resp.errorMessage + "\nДанные не были сохранены");
                }
            }});
        }

        $dialog.dialog({
            autoOpen : false,
            modal : true,
            buttons : [{text : "Ok", click: function() {
                saveRent();
                $( this ).dialog( "close" );
            }}, {text : "Cancel", click : function () {
                $( this ).dialog( "close" );
            }}]
        });

        dialog.open = function (rent) {
            $priceDiv.empty();
            if (rent) {
                rentId = rent.id;
                $markSelectSave.val(rent.car.model.mark.id).change();
                setTimeout(function () {
                    $modelSelectSave.val(rent.car.model.id).change();
                }, 100);
                setTimeout(function () {
                    $carSelectSave.val(rent.car.id);
                }, 150);
                if(rent.tariffPlan)
                    setTimeout(function () {
                       $tariffPlanSelect.val(rent.tariffPlan.id)
                    }, 200);
                $dialog.find('.client').val(rent.client.id);
                $dialog.find('.dateFrom').val(formatDate(rent.dateFrom));
                if(rent.dateTo)
                    $dialog.find('.dateTo').val(formatDate(rent.dateTo));
                $dialog.find('.rentPointFrom').val(rent.rentPointFrom.id);
                if(rent.rentPointTo)
                    $dialog.find('.rentPointTo').val(rent.rentPointTo.id);
                $dialog.find('.status').val(rent.status.id);
                $dialog.find('.pledge').val(rent.pledge);
            }
            else {
                rentId = null;
                $markSelectSave.val("").change();
                $modelSelectSave.val("").change();
                $carSelectSave.val("");
                $dialog.find('.client').val("");
                $dialog.find('.dateFrom').val("");
                $dialog.find('.dateTo').val("");
                $dialog.find('.rentPointFrom').val("");
                $dialog.find('.rentPointTo').val("");
                $dialog.find('.status').val("");
                $dialog.find('.pledge').val("");
            }

            $dialog.dialog("open");
        }

        dialog.isNew = function () {
            return !rentId;
        }

        return dialog;
    }();

    // Форматирование даты
    function formatDate(date) {
        if (date == null)
            return '';

        //var d = $.datepicker.parseDate("yy-mm-dd", date.substr(0, 10));
        var d = new Date(date);
        return $.datepicker.formatDate( "dd.mm.yy", d);
    }

    // Отрисовка записи истории
    function renderRent(rent) {
        var $tr = $('<tr><td><a href="javascript:;">' + rent.car.model.mark.name + ' ' + rent.car.model.name + '</a></td>' +
            '<td>' + formatDate(rent.dateFrom) + ' - ' + formatDate(rent.dateTo) + '</td>' +
            '<td>' + rent.rentPointFrom.name + '</td>' +
            '<td>' + (rent.rentPointTo ? rent.rentPointTo.name : '' )+ '</td>' +
            '<td>' + rent.client.lastName + ' ' + rent.client.firstName + ' ' + rent.client.middleName + '</td>' +
            '<td>' + rent.car.number + '</td></tr>');

        $tr.find('a').click(function(){
            saveDialog.open(rent)
        });
        return $tr;
    }

    // Отрисовка истории
    function renderRentList(list) {
        var tBody = $('#history-table').find('tbody').empty();

        if (!list || !list.length)
            return;

        $.each(list, function(_, rent) {
            tBody.append(renderRent(rent));
        });
    }

    // Поиск
    $('#find-button').click(function () {
        $.ajax({url:"/rent", data: {markId : $markSelect.val(),
            modelId : $modelSelect.val(),
            clientId : $clientSelect.val(),
            carId : $carSelect.val(),
            periodFromStart : $('input.dateFromStart').val(),
            periodFromEnd : $('input.dateFromEnd').val(),
            periodToStart : $('input.dateToStart').val(),
            periodToEnd : $('input.dateToEnd').val(),
            rentPointFromId : $rentPointFrom.val(),
            rentPointToId : $rentPointTo.val()}, success : function (resp) {
            renderRentList(resp);
        }});
    });


    $('#add-button').click(function() {
        saveDialog.open(null);
    });

    // Отчет
    var statTab = $('#tabs-report').find('tbody');
    $('a.stat').click(function () {
        statTab.empty();

        $.ajax({url : "/stat", success : function (resp) {
            if(!resp || !resp.length)
                return;


            $.each(resp, function (_, row) {
                var $tr = $('<tr />');
                $.each(row, function (_, cell) {
                    $tr.append('<td>' + cell + '</td>')
                })
                statTab.append($tr);
            })

        }});
    });
} );