package com.rent.database;

import com.rent.models.RentPoint;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
public interface RentPointRepository extends CrudRepository<RentPoint, Integer> {
}
