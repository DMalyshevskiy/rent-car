package com.rent.database;

import com.rent.models.Rent;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.LockModeType;
import java.util.List;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
public interface RentRepository extends CrudRepository<Rent, Integer>, RentRepositorySearch {
    @Lock(LockModeType.OPTIMISTIC)
    Rent findOne(Integer integer);
}
