package com.rent.database;

import com.rent.models.RentStatus;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
public interface RentStatusRepository extends CrudRepository<RentStatus, Integer> {
}
