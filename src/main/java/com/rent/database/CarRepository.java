package com.rent.database;

import com.rent.models.Car;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
@Repository
public interface CarRepository extends CrudRepository<Car, Integer> {
    @Query("select c " +
           "  from Car c " +
           " where (:modelId is null or c.model.id = :modelId) " +
           "   and (:rentPointId is null or c.currentRentPoint.id = :rentPointId) " +
           "   and (:rentPointId is null or not exists(select r " +
           "                                             from Rent r " +
           "                                            where r.car.id = c.id " +
           "                                              and r.dateFrom <= now() " +
           "                                              and (r.dateTo is null or r.dateTo > now())))")
    public List<Car> getCarByModel(@Param("modelId") Integer modelId, @Param("rentPointId") Integer rentPointId);
}
