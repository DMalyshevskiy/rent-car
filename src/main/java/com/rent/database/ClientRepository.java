package com.rent.database;

import com.rent.models.Client;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
public interface ClientRepository extends CrudRepository<Client, Integer> {
}
