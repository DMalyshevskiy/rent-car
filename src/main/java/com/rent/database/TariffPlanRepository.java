package com.rent.database;

import com.rent.models.TariffPlan;
import org.springframework.data.repository.CrudRepository;

public interface TariffPlanRepository extends CrudRepository<TariffPlan, Integer> {
}
