package com.rent.database;

import com.rent.models.RentPoint;
import org.springframework.context.annotation.Bean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class DBManager {
    @PersistenceContext
    EntityManager em;

    public List<Object[]> getStatistic() {
        StringBuffer sql = new StringBuffer("select rp.name, concat(mrk.name, ' ', m.name) as model, avg(to_days(r.date_to) - to_days(r.date_from)) avgDays\n" +
                "  from t_rent r\n" +
                "  join t_car c on c.id = r.car_id\n" +
                "  join t_model m on m.id = c.model_id\n" +
                "  join t_mark mrk on mrk.id = m.mark_id\n" +
                "  join t_rent_point rp on rp.id = r.rent_point_from_id\n" +
                " where r.date_to is not NULL\n" +
                "   and r.date_from is not null\n" +
                "group by rp.name, concat(mrk.name, ' ', m.name)");

        return em.createNativeQuery(sql.toString()).getResultList();
    }
}
