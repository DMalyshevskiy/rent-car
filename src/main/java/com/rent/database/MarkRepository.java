package com.rent.database;

import com.rent.models.Mark;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
public interface MarkRepository extends CrudRepository<Mark, Integer> {
}
