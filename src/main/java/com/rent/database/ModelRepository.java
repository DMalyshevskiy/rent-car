package com.rent.database;

import com.rent.models.Model;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
public interface ModelRepository extends CrudRepository<Model, Integer> {

    @Query("select m from Model m where m.mark.id = :markId")
    public List<Model> findByMarkId(@Param("markId") Integer markId);

}
