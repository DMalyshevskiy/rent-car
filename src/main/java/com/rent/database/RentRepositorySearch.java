package com.rent.database;

import java.util.Date;
import java.util.List;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
public interface RentRepositorySearch {
    List search(Integer markId, Integer modelId, Date periodFromStart, Date periodFromEnd,
                Date periodToStart, Date periodToEnd, Integer rentPointFromId, Integer rentPointToId,
                Integer clientId, Integer carId);
}
