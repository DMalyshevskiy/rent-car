package com.rent.database;

import com.rent.models.Rent;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
public class RentRepositoryImpl implements RentRepositorySearch  {
    @PersistenceContext
    EntityManager em;

    @Override
    public List<Rent> search(Integer markId, Integer modelId, Date periodFromStart, Date periodFromEnd,
                             Date periodToStart, Date periodToEnd, Integer rentPointFromId, Integer rentPointToId,
                             Integer clientId, Integer carId) {

        StringBuffer sql = new StringBuffer("select tr         \n")
                                    .append("  from Rent tr    \n")
                                    .append(" where (:markId is null or tr.car.model.mark.id = :markId) \n")
                                    .append("   and (:modelId is null or tr.car.model.id = :modelId) \n")
                                    .append("   and (:periodFromStart is null or tr.dateFrom >= :periodFromStart) \n")
                                    .append("   and (:periodFromEnd is null or tr.dateFrom < :periodFromEnd) \n")
                                    .append("   and (:periodToStart is null or tr.dateTo >= :periodToStart) \n")
                                    .append("   and (:periodToEnd is null or tr.dateTo < :periodToEnd) \n")
                                    .append("   and (:rentPointFromId is null or tr.rentPointFrom.id = :rentPointFromId) \n")
                                    .append("   and (:rentPointToId is null or tr.rentPointTo.id = :rentPointToId) \n")
                                    .append("   and (:clientId is null or tr.client.id = :clientId) \n")
                                    .append("   and (:carId is null or tr.car.id = :carId)");

        Query query = em.createQuery(sql.toString());

        query.setParameter("markId", markId);
        query.setParameter("modelId", modelId);
        query.setParameter("periodFromStart",periodFromStart);
        query.setParameter("periodFromEnd",periodFromEnd);
        query.setParameter("periodToStart",periodToStart);
        query.setParameter("periodToEnd",periodToEnd);
        query.setParameter("rentPointFromId", rentPointFromId);
        query.setParameter("rentPointToId", rentPointToId);
        query.setParameter("clientId", clientId);
        query.setParameter("carId", carId);

       return query.getResultList();
    }
}
