package com.rent.database;

import com.rent.models.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
public interface UserRepository extends CrudRepository<User, Integer> {
}
