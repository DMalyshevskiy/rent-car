package com.rent.models;
import javax.persistence.*;

@Entity
@Table(name = "T_CLIENT")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Column(name = "DRIVING_LICENSE_NUMBER")
    private Integer drivingLicenseNumber;

    @Column(name = "DRIVING_LICENSE_SERIA")
    private Integer drivingLicenseSeria;

    public Client() {
    }

    public Client(Integer id, String lastName, String firstName, String middleName, Integer drivingLicenseNumber, Integer drivingLicenseSeria) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.drivingLicenseNumber = drivingLicenseNumber;
        this.drivingLicenseSeria = drivingLicenseSeria;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Integer getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(Integer drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
    }

    public Integer getDrivingLicenseSeria() {
        return drivingLicenseSeria;
    }

    public void setDrivingLicenseSeria(Integer drivingLicenseSeria) {
        this.drivingLicenseSeria = drivingLicenseSeria;
    }
}
