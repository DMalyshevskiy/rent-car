package com.rent.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
@Entity
@Table(name="T_MODEL")
public class Model {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARK_ID", referencedColumnName = "ID")
    private Mark mark;

    private String name;

    @ManyToMany
    @JoinTable(name = "t_model_tariff_plan", joinColumns = @JoinColumn(name = "modelId"), inverseJoinColumns = @JoinColumn(name = "tariff_plan_id"))
    private Collection<TariffPlan> tariffPlans;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Mark getMark() {
        return mark;
    }

    public void setMark(Mark mark) {
        this.mark = mark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<TariffPlan> getTariffPlans() {
        return tariffPlans;
    }

    public void setTariffPlans(Collection<TariffPlan> tariffPlans) {
        this.tariffPlans = tariffPlans;
    }
}
