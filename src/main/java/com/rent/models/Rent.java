package com.rent.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */

@Entity
@Table(name = "T_RENT")
public class Rent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Version
    long version;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CAR_ID", referencedColumnName = "ID")
    private Car car;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CLIENT_ID", referencedColumnName = "ID")
    private Client client;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RENT_POINT_FROM_ID", referencedColumnName = "ID")
    private RentPoint rentPointFrom;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RENT_POINT_TO_ID", referencedColumnName = "ID")
    private RentPoint rentPointTo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_FROM")
    private Date dateFrom;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_TO")
    private Date dateTo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_CREATE_ID", referencedColumnName = "ID")
    private User userCreate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_EDIT_ID", referencedColumnName = "ID")
    private User userEdit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "ID")
    private RentStatus status;

    private Integer pledge;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TARIFF_PLAN_ID", referencedColumnName = "ID")
    private TariffPlan tariffPlan;

    public Rent() {
    }

    public Rent(Car car, Client client, RentPoint rentPointFrom, RentPoint rentPointTo, Date dateFrom, Date dateTo,
                User userCreate, User userEdit, RentStatus status, Integer pledge, TariffPlan tariffPlan) {
        this.car = car;
        this.client = client;
        this.rentPointFrom = rentPointFrom;
        this.rentPointTo = rentPointTo;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.userCreate = userCreate;
        this.userEdit = userEdit;
        this.status = status;
        this.pledge = pledge;
        this.tariffPlan = tariffPlan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public RentPoint getRentPointFrom() {
        return rentPointFrom;
    }

    public void setRentPointFrom(RentPoint rentPointFrom) {
        this.rentPointFrom = rentPointFrom;
    }

    public RentPoint getRentPointTo() {
        return rentPointTo;
    }

    public void setRentPointTo(RentPoint rentPointTo) {
        this.rentPointTo = rentPointTo;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public User getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(User userCreate) {
        this.userCreate = userCreate;
    }

    public User getUserEdit() {
        return userEdit;
    }

    public void setUserEdit(User userEdit) {
        this.userEdit = userEdit;
    }

    public RentStatus getStatus() {
        return status;
    }

    public void setStatus(RentStatus status) {
        this.status = status;
    }

    public Integer getPledge() {
        return pledge;
    }

    public void setPledge(Integer pledge) {
        this.pledge = pledge;
    }

    public TariffPlan getTariffPlan() {
        return tariffPlan;
    }

    public void setTariffPlan(TariffPlan tariffPlan) {
        this.tariffPlan = tariffPlan;
    }
}
