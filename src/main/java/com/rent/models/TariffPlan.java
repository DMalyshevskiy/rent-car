package com.rent.models;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "t_tariff_plan")
public class TariffPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    String name;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_begin")
    Date dateBegin;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_end")
    Date dateEnd;


    @OneToMany(mappedBy = "tariffPlan")
    Collection<Price> priceList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Collection<Price> getPriceList() {
        return priceList;
    }

    public void setPriceList(Collection<Price> priceList) {
        this.priceList = priceList;
    }

}
