package com.rent.models;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
@Entity
@Table(name = "T_MARK")
public class Mark {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
