package com.rent.models;

import org.hibernate.mapping.Join;

import javax.persistence.*;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */

@Entity
@Table(name = "T_CAR")
@SecondaryTable(name = "T_CAR_RENT_POINT", pkJoinColumns = @PrimaryKeyJoinColumn(name = "CAR_ID", referencedColumnName = "ID"))
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MODEL_ID", referencedColumnName = "ID")
    private Model model;

    @Column(name = "CAR_NUMBER")
    private String number;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(table = "T_CAR_RENT_POINT", name = "RENT_POINT_ID", referencedColumnName = "ID")
    private RentPoint currentRentPoint;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public RentPoint getCurrentRentPoint() {
        return currentRentPoint;
    }

    public void setCurrentRentPoint(RentPoint currentRentPoint) {
        this.currentRentPoint = currentRentPoint;
    }
}
