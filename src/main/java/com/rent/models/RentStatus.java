package com.rent.models;

import javax.persistence.*;

/**
 * Created by DMalyshevsky on 27.11.2017.
 */
@Entity
@Table(name = "TD_RENT_STATUS")
public class RentStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
