package com.rent.controllers;

import com.rent.database.MarkRepository;
import com.rent.models.Mark;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MarkController {

    private final MarkRepository markRepository;

    public MarkController(MarkRepository markRepository) {
        this.markRepository = markRepository;
    }

    @RequestMapping("/mark")
    public Iterable<Mark> getListMarks() {
        return markRepository.findAll();
    }
}
