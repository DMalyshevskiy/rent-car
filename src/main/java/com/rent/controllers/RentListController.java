package com.rent.controllers;

import com.rent.database.RentRepository;
import com.rent.models.Rent;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by DMalyshevsky on 27.11.2017.
 */
@RestController
public class RentListController {
    private final RentRepository rentRepository;

    private final Log log = LogFactory.getLog(RentRepository.class);

    private final DateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    public RentListController(RentRepository rentRepository) {
        this.rentRepository = rentRepository;
    }

    @RequestMapping("/rent")
    public Iterable<Rent> getListRentPoints(@RequestParam(name = "markId", required = false) Integer markId,  @RequestParam(name = "modelId", required = false) Integer modelId,
                                            @RequestParam(name = "periodFromStart", required = false) String periodFromStart, @RequestParam(name = "periodFromEnd", required = false) String periodFromEnd,
                                            @RequestParam(name = "periodToStart", required = false) String periodToStart, @RequestParam(name = "periodToEnd", required = false) String periodToEnd,
                                            @RequestParam(name = "rentPointFromId", required = false) Integer rentPointFromId, @RequestParam(name = "rentPointToId", required = false) Integer rentPointToId,
                                            @RequestParam(name = "clientId", required = false) Integer clientId, @RequestParam(name = "carId", required = false) Integer carId) {


        log.info("markId: " + markId + " modelId: " + modelId + "periodFromStart: " + periodFromStart + "periodFromEnd: " + periodFromEnd +
                "periodToStart: " + periodToStart + "periodToEnd: " + periodToEnd + "rentPointFromId: " + rentPointFromId + "rentPointToId: " + rentPointToId +
                "clientId: " + clientId + "carId: " + carId);

        try {

            Date dateFromStart = StringUtils.isBlank(periodFromStart) ? null : format.parse(periodFromStart);
            Date dateFromEnd = StringUtils.isBlank(periodFromEnd) ? null : format.parse(periodFromEnd);
            Date dateToStart = StringUtils.isBlank(periodToStart) ? null : format.parse(periodToStart);
            Date dateToEnd = StringUtils.isBlank(periodToEnd) ? null : format.parse(periodToEnd);

            return rentRepository.search(markId, modelId, dateFromStart, dateFromEnd, dateToStart, dateToEnd,
                    rentPointFromId, rentPointToId, clientId, carId);

        } catch (Exception e) {
            log.error("Ошибка при поиске записей аренды", e);
            System.out.println(e);
            return null;
        }
    }
}
