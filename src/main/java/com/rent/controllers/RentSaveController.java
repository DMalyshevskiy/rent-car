package com.rent.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rent.database.*;
import com.rent.models.Car;
import com.rent.models.Rent;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by DMalyshevsky on 29.11.2017.
 */
@RestController
public class RentSaveController {

    private static final Log log = LogFactory.getLog(RentSaveController.class);

    private static final Integer RENT_STATUS_DONE = 2;
    private final DateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    private final RentRepository rentRepository;
    private final ClientRepository clientRepository;
    private final RentPointRepository rentPointRepository;
    private final RentStatusRepository statusRepository;
    private final UserRepository userRepository;
    private final CarRepository carRepository;
    private final TariffPlanRepository tariffPlanRepository;

    public RentSaveController(RentRepository rentRepository, ClientRepository clientRepository, RentPointRepository rentPointRepository,
                              RentStatusRepository statusRepository, UserRepository userRepository, CarRepository carRepository, TariffPlanRepository tariffPlanRepository) {
        this.rentRepository = rentRepository;
        this.clientRepository = clientRepository;
        this.rentPointRepository = rentPointRepository;
        this.statusRepository = statusRepository;
        this.userRepository = userRepository;
        this.carRepository = carRepository;
        this.tariffPlanRepository = tariffPlanRepository;
    }

    @RequestMapping(value = "/save-rent", produces = "application/json;charset=UTF-8")
    public String saveRent(@RequestParam(name = "rentId", required = false) Integer rentId, @RequestParam(name = "clientId", required = false) Integer clientId,
                           @RequestParam(name = "dateFrom", required = false) String dateFrom, @RequestParam(name = "dateTo", required = false) String dateTo,
                           @RequestParam(name = "rentPointFromId", required = false) Integer rentPointFromId, @RequestParam(name = "rentPointToId", required = false) Integer rentPointToId,
                           @RequestParam(name = "status", required = false) Integer statusId, @RequestParam(name = "carId", required = false) Integer carId,
                           @RequestParam(name = "userCreateId", required = false) Integer userCreateId, @RequestParam(name = "userEditId", required = false) Integer userEditId,
                           @RequestParam(name = "pledge", required = false) Integer pledge, @RequestParam(name = "tariffPlanId", required = false) Integer tariffPlanId){

        Map<String, Object> result = new HashMap<>();

        log.info("/save-rent: rentId: " + rentId +  ", clientId: " + clientId + ", dateFrom: " + dateFrom + ", dateTo: " + dateTo +
                ", rentPointFromId: " + rentPointFromId + ", rentPointToId: " + rentPointToId +
                ", status: " + statusId + ", carId: " + carId +
                ", userCreateId: " + userCreateId + ", userEditId: " + userEditId +
                ", pledge: " + pledge + ", tariffPlanId: " + tariffPlanId);

        try {
            Date dateBegin = StringUtils.isBlank(dateFrom) ? null : format.parse(dateFrom);
            Date dateEnd = StringUtils.isBlank(dateTo) ? null : format.parse(dateTo);

            if(clientId == null || dateBegin == null || rentPointFromId == null || statusId == null || carId == null)
                throw new IllegalArgumentException("Не заполнены необходимые поля");

            Rent rent;
            if(rentId != null) {
                rent = rentRepository.findOne(rentId);
            } else {
                rent = new Rent();
            }

            rent.setClient(clientRepository.findOne(clientId));
            rent.setDateFrom(dateBegin);
            rent.setDateTo(dateEnd);
            rent.setRentPointFrom(rentPointRepository.findOne(rentPointFromId));
            if(rentPointToId != null)
                rent.setRentPointTo(rentPointRepository.findOne(rentPointToId));
            rent.setStatus(statusRepository.findOne(statusId));

            Car car = carRepository.findOne(carId);
            if(RENT_STATUS_DONE.equals(rent.getStatus().getId())) {
                car.setCurrentRentPoint(rent.getRentPointTo());
            }

            rent.setCar(car);
            rent.setPledge(pledge);
            if(tariffPlanId != null)
                rent.setTariffPlan(tariffPlanRepository.findOne(tariffPlanId));

            rent.setUserCreate(userRepository.findOne(userCreateId));
            rent.setUserEdit(userRepository.findOne(userEditId));

            rentRepository.save(rent);

           result.put("isError", false);
           result.put("rentId", rent.getId());

        } catch (Exception e) {
            log.error("Ошибка при сохранении записи аренды");
            result.put("isError", true);
            result.put("errorMessage", StringUtils.isBlank(e.getMessage()) ? "Неизвестная ошибка" : e.getMessage());
        }


        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            log.error(e);
            return "{isError : true, errorMessage : \"Неизвестная ошибка\"}";
        }

    }
}
