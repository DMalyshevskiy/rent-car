package com.rent.controllers;

import com.rent.database.RentStatusRepository;
import com.rent.models.RentStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatusController {

    private final RentStatusRepository rentStatusRepository;

    public StatusController(RentStatusRepository rentStatusRepository) {
        this.rentStatusRepository = rentStatusRepository;
    }

    @RequestMapping("/status")
    Iterable<RentStatus> getListStatus() {
        return rentStatusRepository.findAll();
    }
}
