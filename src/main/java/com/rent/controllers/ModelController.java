package com.rent.controllers;

import com.rent.database.ModelRepository;
import com.rent.models.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ModelController {

    private final ModelRepository modelRepository;

    public ModelController(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    @RequestMapping("/model")
    public Iterable<Model> getModelList(@RequestParam(name = "markId", required = false) Integer markId) {
        if (markId == null)
            return modelRepository.findAll();
        else
            return modelRepository.findByMarkId(markId);
    }
}
