package com.rent.controllers;

import com.rent.database.CarRepository;
import com.rent.models.Car;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarController {

    private final CarRepository carRepository;

    public CarController(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @RequestMapping("/car")
    public Iterable<Car> getCars(@RequestParam(name = "modelId", required = false) Integer modelId,
                                 @RequestParam(name = "rentPointId", required = false) Integer rentPointId,
                                 @RequestParam(name = "rentPointRequired", required = false) Boolean rentPointRequired) {

        if(Boolean.TRUE.equals(rentPointRequired) && rentPointId == null)
            return null;

        return carRepository.getCarByModel(modelId, rentPointId);
    }
}
