package com.rent.controllers;

import com.rent.database.ClientRepository;
import com.rent.models.Client;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {

    private final ClientRepository clientRepository;


    public ClientController(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @RequestMapping("/clients")
    public Iterable<Client> getListClients() {
        return clientRepository.findAll();
    }
}
