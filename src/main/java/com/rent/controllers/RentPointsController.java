package com.rent.controllers;

import com.rent.database.RentPointRepository;
import com.rent.models.RentPoint;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RentPointsController {

    private final RentPointRepository rentPointRepository;

    public RentPointsController(RentPointRepository rentPointRepository) {
        this.rentPointRepository = rentPointRepository;
    }


    @RequestMapping("/rentpoints")
    public Iterable<RentPoint> getListRentPoints() {
        return rentPointRepository.findAll();
    }
}
