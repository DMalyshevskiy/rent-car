package com.rent;

import com.rent.controllers.RentSaveController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RentSaveControllerTest {


    @Autowired
    private RentSaveController controllerToTest;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(controllerToTest).build();
    }

    @Test
    public void testSaveRent() throws Exception{
        //Mocking Controller
        controllerToTest = mock(RentSaveController.class);

        mockMvc.perform(post("/save-rent")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .param("rentId", "9")
                .param("clientId", "1")
                .param("dateFrom", "01.01.2017")
                .param("dateTo", "31.01.2017")
                .param("rentPointFromId", "1")
                .param("rentPointToId", "2")
                .param("status", "1")
                .param("carId", "2")
                .param("userCreateId", "1")
                .param("userEditId", "1")
                .param("pledge", "0")
                .param("tariffPlanId", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.rentId", is(9)));
    }
    @Test
    public void failSaveRent() throws Exception{
        //Mocking Controller
        controllerToTest = mock(RentSaveController.class);

        mockMvc.perform(post("/save-rent")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .param("rentId", "9"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.isError", is(true)));
    }
}
